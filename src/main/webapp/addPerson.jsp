<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="person" class="com.example.servletjspdemo.domain.Person" scope="session" />

<jsp:setProperty name="person" property="*" /> 

<jsp:useBean id="storage" class="com.example.servletjspdemo.service.StorageService" scope="application" />

<%

storage.connect();
if(storage.getSize()<5){
 if(new String(request.getParameter("email")).equals(request.getParameter("cemail"))){
	out.println("<p>Following person has been added to storage: </p>"+
			"<p>Name:" + person.getFirstName() +"</p>"+
			"<p>Surname:" + person.getSurName() +"</p>"+
			"<p>Email:" + person.getEmail() +"</p>"+
			"<p>Employer:" + person.getEmployer() +"</p>" +
			"<p>How did you find out ?:" + person.getHow() +"</p>" +
			"<p>Occupation:" + person.getOcup() +"</p>");
  storage.adddb(person);
  out.println("<p><a href='showAllPersons.jsp'>See whoe else singed up</a></p>" + 
  				"<p><a href='index.jsp'>Main Page</a></p>");
 }
 else {
	 request.getSession().invalidate();
		out.println("<p><a href='getPersonData.jsp'>Emails do not match please fill the form again!!</a></p>");
 }
}else {
	out.println("<p>There are no free spots! Sorry!!</p>" + 
				"<p><a href='index.jsp'>Main Page</a></p>");
	
}
%>

</body>
</html>