package com.example.servletjspdemo.domain;

public class Person {
	
	private int id;
	private String firstName = "";
	private String surName = "";
	private String email = " ";
	private String cemail = " ";
	private String employer = "";
	private String how = "";
	private String ocup = "";
	
	
	public Person() {
		super();
	}
	
	public Person(String firstName, String surName, String email, String employer, String how, String ocup) {
		super();
		this.firstName = firstName;
		this.surName = surName;
		this.email = email;
		this.employer = employer;
		this.how = how;
		this.ocup = ocup;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCemail() {
		return cemail;
	}

	public void setCemail(String cemail) {
		this.cemail = cemail;
	}

	public String getEmployer() {
		return employer;
	}

	public void setEmployer(String employer) {
		this.employer = employer;
	}

	public String getHow() {
		return how;
	}

	public void setHow(String how) {
		this.how = how;
	}

	public String getOcup() {
		return ocup;
	}

	public void setOcup(String ocup) {
		this.ocup = ocup;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
