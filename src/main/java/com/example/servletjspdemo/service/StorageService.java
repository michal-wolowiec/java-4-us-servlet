package com.example.servletjspdemo.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.servletjspdemo.domain.Person;

public class StorageService {
	
	private List<Person> db = new ArrayList<Person>();
	
	private Connection connection;
	
	private String url = "jdbc:hsqldb:hsql://localhost/workdb";
	
	private PreparedStatement addPersonStmt;
	private PreparedStatement getAllPersonsStmt;
	
	private Statement statement;
	
	public void connect(){
		
		try{
			Class.forName("org.hsqldb.jdbc.JDBCDriver" );
		}catch(Exception e){
			System.err.println("ERROR: failed to load HSQLDB JDBC driver.");
		     e.printStackTrace();
		}
		
		try{
			connection = DriverManager.getConnection(url, "SA", "");
			statement = connection.createStatement();
			
		//	ResultSet rs = connection.getMetaData().getTables(null, null, null, null);
		
		addPersonStmt = connection.prepareStatement("INSERT INTO Person(firstname,surname,email,employer,how,occupation) VALUES(?, ?, ?, ?, ?, ?)");
		getAllPersonsStmt = connection.prepareStatement("SELECT id, firstname,surname,email,employer,how,occupation FROM Person");
		
		} catch(SQLException e){
			e.printStackTrace();
		}
	}
	

	
	public void adddb(Person person){
		int count = 0;
		try {
			addPersonStmt.setString(1, person.getFirstName());
			addPersonStmt.setString(2, person.getSurName());
			addPersonStmt.setString(3, person.getEmail());
			addPersonStmt.setString(4, person.getEmployer());
			addPersonStmt.setString(5, person.getHow());
			addPersonStmt.setString(6, person.getOcup());
			
			count = addPersonStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public Connection getConnection(){
		return connection;
	}

	
	public int howMany()
	{
		int many = 0;
		try{
			ResultSet rs = getAllPersonsStmt.executeQuery();
			
			while(rs.next()){
				many++;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return many;
	}
	
	
	public List<Person> getAllPersons() {
		
		
		db.clear();
		try{
			ResultSet rs = getAllPersonsStmt.executeQuery();
			
			while(rs.next()){
				Person p = new Person();
				p.setId(rs.getInt("id"));
				p.setFirstName(rs.getString("firstname"));
				p.setSurName(rs.getString("surname"));
				p.setEmail(rs.getString("email"));
				p.setEmployer(rs.getString("employer"));
				p.setHow(rs.getString("how"));
				p.setOcup(rs.getString("occupation"));
				db.add(p);
			} 
		} catch(SQLException e){
			e.printStackTrace();
		}
		return db;
	}
	
	public int getSize(){
		return db.size();
	}
}
